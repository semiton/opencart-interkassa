<?php

/**
 * Class ControllerPaymentInterkassa
 *
 * @property ModelCheckoutOrder model_checkout_order
 * @property ModelPaymentInterkassa model_payment_interkassa
 * @property Config config
 * @property Loader load
 * @property Language language
 * @property Currency currency
 * @property Session session
 * @property Request request
 * @property Response response
 */
class ControllerPaymentInterkassa extends Controller
{
	protected function index()
    {
		$this->data['button_confirm'] = $this->language->get('button_confirm');

		$this->load->model('checkout/order');
		$this->load->model('payment/interkassa');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		$this->data['action'] =  $this->config->get('interkassa_action');
        $this->data['charset'] = $this->config->get('interkassa_charset');
		$this->data['ik_co_id'] = $this->config->get('interkassa_merchant');
		$this->data['ik_am'] = (float) $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);
		$this->data['ik_pm_no'] = $this->model_payment_interkassa->createPayment($this->config->get('interkassa_merchant'), $this->session->data['order_id'], $this->data['ik_am']);
		$this->data['ik_desc'] = "#".$this->session->data['order_id']."@".$this->data['ik_pm_no'];

        $ik_params = [
            'ik_co_id' => $this->data['ik_co_id'],
            'ik_am' => $this->data['ik_am'],
            'ik_pm_no' => $this->data['ik_pm_no'],
            'ik_desc' => $this->data['ik_desc'],
        ];
        $ik_sign = array_values($ik_params);
        ksort($ik_sign, SORT_STRING);
        $secret_key = $this->config->get('interkassa_security');
        $ik_sign[] = $secret_key;
        $ik_sign = base64_encode(md5(implode(':', $ik_sign), true));
        $this->data['ik_sign'] = $ik_sign;

		if(file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/interkassa.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/interkassa.tpl';
		} else {
			$this->template = 'default/template/payment/interkassa.tpl';
		}

		$this->render();
	}

    public static function filterInputParams($param)
    {
        return preg_match('/^(ik_)(.+)$/', $param);
    }

	public function callback()
    {
		$callback_method = $this->config->get('interkassa_callback_method');
        $params = $callback_method === 'GET' ? $this->request->get : $this->request->post;
        $params = array_flip(array_filter(array_flip($params), 'ControllerPaymentInterkassa::filterInputParams'));
        if (empty($params['ik_co_id']) or empty($params['ik_pm_no']) or empty($params['ik_am'])) {
            throw new ErrorException("Invalid params", 400);
        }


        $this->load->model('payment/interkassa');
        $payment_info = $this->model_payment_interkassa->getPaymentById($params['ik_pm_no']);
        if(count((array) $payment_info) === 0 or in_array($params['ik_inv_st'], array('success', 'fail'))) {
            throw new ErrorException('Payment not found', 404);
        }
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($payment_info['order_id']);
        if(!$order_info) {
            throw new ErrorException('Order not found', 404);
        }

        if (isset($params['ik_sign'])) {
            if((float) $params['ik_am'] !== (float) $payment_info['ik_am']) {
                throw new ErrorException('Invalid sign hash', 400);
            }
            $secret_key = $this->config->get('interkassa_security');
            $ik_sign = $params['ik_sign'];
            unset($params['ik_sign']);
            ksort($params, SORT_STRING);
            $params[] = $secret_key;
            $ik_sign_result = base64_encode(md5(implode(':', array_values($params)), true));
            if ($ik_sign_result !== $ik_sign) {
                throw new ErrorException('Invalid sign hash', 400);
            }
            foreach ($params as $key => $value) {
                if (isset($payment_info[$key])) {
                    $payment_info[$key] = $value;
                }
            }
            $this->model_payment_interkassa->updatePayment($params['ik_pm_no'], $params);
            if ($params['ik_inv_st'] === 'success') {
                $this->model_checkout_order->confirm($payment_info['order_id'], $this->config->get('interkassa_order_status_id'));
            }
            echo "OK";
        } else {
            $this->response->redirect($this->url->link('checkout/success'));
        }
	}
}
