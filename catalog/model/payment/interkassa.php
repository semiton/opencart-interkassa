<?php

/**
 * Class ModelPaymentInterkassa
 *
 * @property DB db
 * @property Config config
 * @property Loader load
 * @property Language language
 * @property ModelSettingSetting model_setting_setting
 */
class ModelPaymentInterkassa extends Model
{
    /**
     * @param array $address
     * @param float $total
     * @return array
     */
    public function getMethod($address, $total)
    {
		$this->load->language('payment/interkassa');
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."zone_to_geo_zone WHERE geo_zone_id = '".(int)$this->config->get('interkassa_geo_zone_id')."' AND country_id = '".(int)$address['country_id']."' AND (zone_id = '".(int)$address['zone_id']."' OR zone_id = '0')");
		
		if ($this->config->get('interkassa_total') > $total) {
			$status = false;
		} elseif (!$this->config->get('interkassa_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
        }

		$method_data = array();

		if ($status) {  
      		$method_data = array( 
        		'code' => 'interkassa',
        		'title' => $this->language->get('text_title'),
				'sort_order' => $this->config->get('interkassa_sort_order')
      		);
    	}

    	return $method_data;
  	}

    /**
     * @param string|null $where
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return object
     */
    public function _getPayments($where = null, array $order = array('ik_pm_no' => 'DESC'), $limit = 20, $offset = 0)
    {
        $sql = "
            SELECT `".DB_PREFIX."interkassa_payments`.*, `".DB_PREFIX."order`.* FROM `".DB_PREFIX."interkassa_payments`
            INNER JOIN `".DB_PREFIX."order` ON `".DB_PREFIX."interkassa_payments`.`order_id` = `".DB_PREFIX."order`.`order_id`";
        if ($where !== null) {
            $sql .= " WHERE ".$where;
        }
        $order_count = count($order);
        if ($order_count > 0) {
            $sql .= " ORDER BY ";
            $i = 1;
            foreach ($order as $order_by => $sort) {
                $sql .= $order_by." ".$sort.($i < $order_count ? ", " : "");
            }
        }
        $sql .= " LIMIT ".(int) $limit." OFFSET ".(int) $offset;

        return $this->db->query($sql);
    }

    /**
     * @param string|null $where
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getPayments($where = null, array $order = array('ik_pm_no' => 'DESC'), $limit = 20, $offset = 0)
    {
        return $this->_getPayments($where, $order, $limit, $offset)->rows;
    }

    /**
     * @param string|null $where
     * @param array $order
     * @return false|array
     */
    public function getPayment($where = null, array $order = array('ik_pm_no' => 'DESC'))
    {
        $result = $this->_getPayments($where, $order, 1, 0)->row;
        if (count($result) === 0) {
            return false;
        }
        return $result;
    }

    /**
     * @param string $id
     * @return array|false
     */
    public function getPaymentById($id)
    {
        return $this->getPayment("`ik_pm_no` = '".$this->db->escape($id)."'");
    }

    /**
     * @param int $order_id
     * @return array|false
     */
    public function getPaymentByOrderId($order_id)
    {
        return $this->getPayment(DB_PREFIX."interkassa_payments`.`order_id` = '".$this->db->escape($order_id)."'");
    }

    /**
     * @param int $order_id
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getPaymentsByOrderId($order_id, array $order = array('ik_pm_no' => 'DESC'), $limit = 20, $offset = 0)
    {
        $this->getPayments(DB_PREFIX."interkassa_payments`.`order_id` = '".$this->db->escape($order_id)."'", $order, $limit, $offset);
    }

    /**
     * @param string $ik_co_id
     * @param int $order_id
     * @param float $ik_am
     * @return string $ik_pm_no
     */
    public function createPayment($ik_co_id, $order_id, $ik_am)
    {
        $this->db->query("INSERT INTO `".DB_PREFIX."interkassa_payments` SET `ik_co_id` = '".$this->db->escape($ik_co_id)."', `order_id` = '".(int)$order_id."', `ik_am` = '".(float)$ik_am."'");
        return (string) $this->db->getLastId();
    }

    /**
     * @param string $ik_pm_no
     * @param array $values
     * @return bool
     */
    public function updatePayment($ik_pm_no, array $values)
    {
        if (count($values) === 0) return false;
        $update = array();
        foreach ($values as $key => $value) {
            $update[] = " `".$key."` = '".$this->db->escape($value)."'";
        }
        $sql = "UPDATE `".DB_PREFIX."interkassa_payments` SET ".implode(",")." WHERE `ik_pm_no` = '".$this->db->escape($ik_pm_no)."'";
        return $this->db->query($sql);
    }
}
