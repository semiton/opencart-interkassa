<?php
define('DS', DIRECTORY_SEPARATOR);
// HTTP
define('HTTP_SERVER', "http://".$_SERVER['HTTP_HOST']."/admin/");
define('HTTP_CATALOG', "http://".$_SERVER['HTTP_HOST']."/");

// HTTPS
define('HTTPS_SERVER', "https://".$_SERVER['HTTP_HOST']."/admin/");
define('HTTPS_CATALOG', "https://".$_SERVER['HTTP_HOST']."/");

// DIR
define('DIR_BASE', dirname(dirname(__FILE__)).DS);
define('DIR_APPLICATION', DIR_BASE."admin".DS);
define('DIR_SYSTEM', DIR_BASE."system".DS);
define('DIR_DATABASE', DIR_SYSTEM."database".DS);
define('DIR_LANGUAGE', DIR_APPLICATION."language".DS);
define('DIR_TEMPLATE', DIR_APPLICATION."view".DS."template".DS);
define('DIR_CONFIG', DIR_SYSTEM."config".DS);
define('DIR_IMAGE', DIR_BASE."image".DS);
define('DIR_CACHE', DIR_SYSTEM."cache".DS);
define('DIR_DOWNLOAD', DIR_BASE."download".DS);
define('DIR_LOGS', DIR_SYSTEM."logs".DS);
define('DIR_CATALOG', DIR_BASE."catalog".DS);

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'opencart');
define('DB_PASSWORD', 'opencart');
define('DB_DATABASE', 'opencart');
define('DB_PREFIX', 'oc_');
