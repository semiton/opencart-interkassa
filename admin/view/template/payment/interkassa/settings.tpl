<?php echo $header; ?>
<div id="content">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb): ?>
		    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php endforeach; ?>
	</div>
	<?php if (isset($errors['warning'])): ?>
	    <div class="warning"><?php echo $errors['warning']; ?></div>
	<?php endif; ?>
	<div class="box">
		<div class="heading">
			<h1><img src="view/image/payment/interkassa.png" alt="" /> <?php echo $heading_title; ?> - <?php echo $heading_title_settings; ?></h1>
			<div class="buttons">
                <a href="<?php echo $this->url->link('payment/interkassa/payments', 'token=' . $this->session->data['token'], 'SSL'); ?>" class="button"><span><?php echo $button_payments; ?></span></a>
                <a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a>
                <a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a>
            </div>
		</div>
		<div class="content">
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="form">
					<tr>
						<td><?php echo $entry_status; ?></td>
						<td>
                            <select name="interkassa_status" required="required">
                                <option value="1"<?php if ($interkassa_status == 1) echo ' selected="selected"'; ?>><?php echo $text_enabled; ?></option>
                                <option value="0"<?php if ($interkassa_status == 0) echo ' selected="selected"'; ?>><?php echo $text_disabled; ?></option>
						    </select>
                        </td>
					</tr>
					<tr>
						<td><?php echo $entry_order_status; ?></td>
						<td>
                            <select name="interkassa_order_status_id" required="required">
							<?php foreach ($order_statuses as $order_status): ?>
								<option value="<?php echo $order_status['order_status_id']; ?>"<?php if ($order_status['order_status_id'] == $interkassa_order_status_id) echo ' selected="selected"'; ?>>
                                    <?php echo $order_status['name']; ?>
                                </option>
							<?php endforeach; ?>
						</select>
                        </td>
					</tr>
					<tr>
						<td><?php echo $entry_geo_zone; ?></td>
						<td><select name="interkassa_geo_zone_id">
							<option value="0"><?php echo $text_all_zones; ?></option>
							<?php foreach ($geo_zones as $geo_zone): ?>
								<option value="<?php echo $geo_zone['geo_zone_id']; ?>" <?php if ($geo_zone['geo_zone_id'] == $interkassa_geo_zone_id) echo ' selected="selected"'; ?>>
                                    <?php echo $geo_zone['name']; ?>
                                </option>
						    <?php endforeach; ?>
						</select></td>
					</tr>
					<tr>
						<td><?php echo $entry_sort_order; ?></td>
						<td><input type="text" name="interkassa_sort_order" value="<?php echo $interkassa_sort_order; ?>" size="1"/></td>
					</tr>
					<tr>
						<td><span class="required">*</span> <?php echo $entry_merchant; ?></td>
						<td>
                            <input type="text" name="interkassa_merchant" value="<?php echo $interkassa_merchant; ?>" required="required"/>
							<?php if (isset($errors['merchant'])): ?><span class="error"><?php echo $errors['merchant']; ?></span><?php endif; ?>
                        </td>
					</tr>
					<tr>
						<td><span class="required">*</span> <?php echo $entry_security; ?></td>
						<td>
                            <input type="text" name="interkassa_security" value="<?php echo $interkassa_security; ?>" required="required"/>
                            <?php if (isset($errors['security'])): ?><span class="error"><?php echo $errors['security']; ?></span><?php endif; ?>
                        </td>
					</tr>
					<tr>
						<td><span class="required">*</span> <?php echo $entry_charset; ?></td>
						<td>
                            <input type="text" name="interkassa_charset" value="<?php echo $interkassa_charset; ?>" required="required"/>
                            <?php if (isset($errors['charset'])): ?><span class="error"><?php echo $errors['charset']; ?></span><?php endif; ?>
                        </td>
					</tr>
					<tr>
						<td><span class="required">*</span> <?php echo $entry_callback_method; ?></td>
						<td>
                            <select name="interkassa_callback_method" required="required">
                                <option value="POST"<?php if ($interkassa_callback_method === 'POST') echo ' selected="selected"'; ?>>POST</option>
                                <option value="GET"<?php if ($interkassa_callback_method === 'GET') echo ' selected="selected"'; ?>>GET</option>
                            </select>
							<?php if (isset($errors['callback_method'])): ?><span class="error"><?php echo $errors['callback_method']; ?></span><?php endif; ?>
                        </td>
					</tr>
					<tr>
						<td><span class="required">*</span> <?php echo $entry_action; ?></td>
						<td>
                            <input type="text" name="interkassa_action" value="<?php echo $interkassa_action; ?>" required="required"/>
                            <?php if (isset($errors['action'])): ?><span class="error"><?php echo $errors['action']; ?></span><?php endif; ?>
                        </td>
					</tr>
					<tr>
						<td><?php echo $entry_callback; ?></td>
						<td><?php echo $callback; ?></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>
<?php echo $footer; ?>