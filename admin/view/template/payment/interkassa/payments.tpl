<?php echo $header; ?>
    <div id="content">
        <div class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php echo $breadcrumb['separator']; ?><a
                href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
        </div>
        <div class="box">
            <div class="heading">
                <h1><img src="view/image/payment/interkassa.png" alt="" /> <?php echo $heading_title; ?> - <?php echo $heading_title_payments; ?></h1>
                <div class="buttons">
                    <a href="<?php echo $this->url->link('payment/interkassa', 'token=' . $this->session->data['token'], 'SSL'); ?>" class="button"><span><?php echo $button_settings; ?></span></a>
                </div>
            </div>
            <div class="content">
                <table class="list">
                    <thead>
                    <tr>
                        <th><?php echo $ik_co_id; ?></th>
                        <th><?php echo $ik_pm_no; ?></th>
                        <th><?php echo $ik_pw_via; ?></th>
                        <th><?php echo $ik_cur; ?></th>
                        <th><?php echo $ik_am; ?></th>
                        <th><?php echo $ik_co_rfn; ?></th>
                        <th><?php echo $ik_inv_crt; ?></th>
                        <th><?php echo $ik_inv_st; ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($payments as $row): ?>
                        <tr>
                            <td><?php echo $row['ik_co_id']; ?></td>
                            <td><?php echo $row['ik_pm_no']; ?></td>
                            <td><?php echo $row['ik_pw_via']; ?></td>
                            <td><?php echo $row['ik_cur']; ?></td>
                            <td><?php echo $row['ik_am']; ?></td>
                            <td><?php echo $row['ik_co_rfn']; ?></td>
                            <td><?php echo $row['ik_inv_crt']; ?></td>
                            <td><?php echo $row['ik_inv_st']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="pagination"><?php echo $pagination; ?></div>
            </div>
        </div>
    </div>
<?php echo $footer; ?>