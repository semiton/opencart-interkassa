<?php

/**
 * Class ControllerPaymentInterkassa
 *
 * @property Request request
 * @property Response response
 * @property Loader load
 * @property Language language
 * @property Document document
 * @property Config config
 * @property Session session
 * @property Url url
 * @property User user
 * @property ModelPaymentInterkassa model_payment_interkassa
 * @property ModelSettingSetting model_setting_setting
 * @property ModelLocalisationOrderStatus model_localisation_order_status
 * @property ModelLocalisationGeoZone model_localisation_geo_zone
 */
class ControllerPaymentInterkassa extends Controller
{
	private $errors = array();
    private $heading_title = array();

    /**
     * @override
     * @param Registry $registry
     */
    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->load->model('setting/setting');
        $this->load->model('payment/interkassa');
        $this->data = array_merge_recursive($this->data, $this->load->language('payment/interkassa'));
        $this->heading_title = [$this->language->get('heading_title')];


        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'text' => $this->language->get('text_home'),
            'separator' => FALSE
        );
        $this->data['breadcrumbs'][] = array(
            'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
            'text' => $this->language->get('text_payment'),
            'separator' => ' :: '
        );
        $this->data['breadcrumbs'][] = array(
            'href' => $this->url->link('payment/interkassa', 'token=' . $this->session->data['token'], 'SSL'),
            'text' => $this->language->get('heading_title'),
            'separator' => ' :: '
        );

        $this->children = array(
            'common/header',
            'common/footer'
        );
    }

    /**
     * @override
     * @return string
     */
    protected function render()
    {
        $this->document->setTitle(implode(' - ', $this->heading_title));
        return parent::render();
    }

    public function install()
    {
        $this->model_payment_interkassa->install();
    }

    public function uninstall()
    {
        $this->model_payment_interkassa->uninstall();
    }

	public function index()
    {
        $this->load->model('localisation/order_status');
        $this->load->model('localisation/geo_zone');
        $this->heading_title[] = $this->language->get('heading_title_settings');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->load->model('setting/setting');
			$this->model_setting_setting->editSetting('interkassa', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
        }

        $replace_params = array(
            'interkassa_merchant',
            'interkassa_security',
            'interkassa_order_status_id',
            'interkassa_charset',
            'interkassa_callback_method',
            'interkassa_action',
            'interkassa_status',
            'interkassa_sort_order',
            'interkassa_geo_zone_id',
        );
        foreach ($replace_params as $param) {
            $this->data[$param] = isset($this->request->post[$param]) ? $this->request->post[$param] : $this->config->get($param);
        }

        $this->data['action'] =$this->url->link('payment/interkassa', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel'] =$this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
        $this->data['callback'] = HTTP_CATALOG . 'index.php?route=payment/interkassa/callback';
		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
        $this->data['errors'] = $this->errors;
		$this->template = 'payment/interkassa/settings.tpl';
		$this->response->setOutput($this->render(true), $this->config->get('config_compression'));
	}

    public function payments()
    {
        $this->heading_title[] = $this->language->get('heading_title_payments');
        $this->data['breadcrumbs'][] = array(
            'href' => $this->url->link('payment/interkassa/payments', 'token='.$this->session->data['token'], 'SSL'),
            'text' => $this->language->get('heading_title_payments'),
            'separator' => ' :: '
        );
        $limit = $this->config->get('config_admin_limit');
        $page = (isset($this->request->get['page']) and $this->request->get['page'] >= 1) ? (int) $this->request->get['page'] : 1;
        $offset = ($page - 1) * $limit;

        $this->data['payments'] = $this->model_payment_interkassa->getPayments(null, array('ik_pm_no' => 'DESC'), $limit, $offset);

        $pagination = new Pagination();
        $pagination->total = $this->model_payment_interkassa->getPaymentsTotal();
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('payment/interkassa/payments', 'token='.$this->session->data['token'].'&page={page}', 'SSL');
        $this->data['pagination'] = $pagination->render();

        $this->template = 'payment/interkassa/payments.tpl';

        $this->response->setOutput($this->render(true), $this->config->get('config_compression'));
    }

	private function validate()
    {
		if (!$this->user->hasPermission('modify', 'payment/interkassa')) {
			$this->errors['warning'] = $this->language->get('error_permission');
		}
		if (strlen((string) $this->request->post['interkassa_merchant']) === 0) {
			$this->errors['merchant'] = $this->language->get('error_merchant');
		}

		if (strlen((string) $this->request->post['interkassa_security']) === 0) {
			$this->errors['security'] = $this->language->get('error_security');
		}
		if (strlen((string) $this->request->post['interkassa_charset']) === 0) {
			$this->errors['charset'] = $this->language->get('error_charset');
		}
		if (strlen((string) $this->request->post['interkassa_callback_method']) === 0) {
			$this->errors['callback_method'] = $this->language->get('error_callback_method');
		}
		if (strlen((string) $this->request->post['interkassa_action']) === 0) {
			$this->errors['action'] = $this->language->get('error_action');
		}

		return count($this->errors) === 0;
	}
}
