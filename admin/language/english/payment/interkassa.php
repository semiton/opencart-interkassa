<?php

// Heading
$_['heading_title'] = 'Interkassa';
$_['heading_title_settings'] = 'Settings';
$_['heading_title_payments'] = 'Payments';

// Text
$_['text_payment'] = 'Payment';
$_['text_success'] = 'Completed: You have changed the settings';
$_['text_interkassa'] = '<a onclick="window.open(\'http://www.interkassa.com\');"><img src="view/image/payment/interkassa.png" alt="http://www.interkassa.com" title="http://www.interkassa.com" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_help']		 = 'help';

// Entry
$_['entry_merchant'] = 'Shop ID:';
$_['entry_security'] = 'Secret key:';
$_['entry_order_status'] = 'Status of the order after payment:';
$_['entry_callback'] 	 = 'Status URL:';
$_['entry_geo_zone'] = 'Geographical zone:';
$_['entry_status'] = 'Status:';
$_['entry_sort_order'] = 'Sort order:';
$_['entry_charset'] = 'Request form charset:';
$_['entry_action'] = 'Request form URL:';
$_['entry_callback_method'] = 'Notification payment method (pending):';

// Error
$_['error_permission'] = 'You have no rights to manage module!';
$_['error_merchant'] = 'Enter the The ID store!';
$_['error_security'] = 'Enter the secret key!';
$_['error_charset'] = 'Enter the charset!';
$_['error_action'] = 'Enter The URL!';
$_['error_callback_method'] = 'Enter "POST" or "GET"!';

// Buttons
$_['button_payments'] = 'Switch to payments';
$_['button_settings'] = 'Switch to settings';

// Interkassa columns
$_['ik_co_id'] = 'Checkout ID';
$_['ik_pm_no'] = 'Payment No.';
$_['ik_desc'] = 'Description';
$_['ik_pw_via'] = 'Payway Via';
$_['ik_am'] = 'Amount';
$_['ik_cur'] = 'Currency';
$_['ik_inv_id'] = 'Invoice Id';
$_['ik_trn_id'] = 'Transaction Id';
$_['ik_inv_crt'] = 'Invoice Created';
$_['ik_inv_prc'] = 'Invoice Processed';
$_['ik_inv_st'] = 'Invoice State';
$_['ik_inv_st_success'] = 'Invoice State';
$_['ik_inv_st_fail'] = 'Invoice State';
$_['ik_inv_st_spending'] = 'Invoice State';
$_['ik_ps_price'] = 'Paysystem Price';
$_['ik_co_rfn'] = 'Checkout Refund';
