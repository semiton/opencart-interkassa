<?php

// Heading
$_['heading_title'] = 'Интеркасса';
$_['heading_title_settings'] = 'Настройки';
$_['heading_title_payments'] = 'Платежи';

// Text
$_['text_payment'] = 'Оплата';
$_['text_success'] = 'Выполнено: Вы изменили настройки';
$_['text_interkassa'] = '<a onclick="window.open(\'http://www.interkassa.com\');"><img src="view/image/payment/interkassa.png" alt="http://www.interkassa.com" title="http://www.interkassa.com" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_help'] = 'справка';

// Entry
$_['entry_merchant'] = 'Идентификатор кассы:';
$_['entry_security'] = 'Секретный ключ:';
$_['entry_order_status'] = 'Статус заказа после оплаты:';
$_['entry_callback'] = 'Status URL:';
$_['entry_geo_zone'] = 'Географическая зона:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Порядок сортировки:';
$_['entry_charset'] = 'Кодировка формы запроса:';
$_['entry_action'] = 'URL формы запроса:';
$_['entry_callback_method'] = 'Метод оповещения платежа:';

// Error
$_['error_permission'] = 'У Вас нет прав для управления модулем!';
$_['error_merchant'] = 'Введите Идентификатор магазина!';
$_['error_security'] = 'Введите Секретный ключ!';
$_['error_charset'] = 'Укажите кодировку запроса!';
$_['error_action'] = 'Укажите URL формы запроса!';
$_['error_callback_method'] = 'Укажите метод оповещения платежа';

// Buttons
$_['button_payments'] = 'Перейти к платежам';
$_['button_settings'] = 'Перейти к настройкам';

// Interkassa columns
$_['ik_co_id'] = 'Идентификатор кассы';
$_['ik_pm_no'] = 'Номер';
$_['ik_desc'] = 'Описание';
$_['ik_pw_via'] = 'Способ оплаты';
$_['ik_am'] = 'Сумма';
$_['ik_cur'] = 'Валюта';
$_['ik_inv_id'] = 'Идентификатор платежа';
$_['ik_trn_id'] = 'Идентификатор транзакции';
$_['ik_inv_crt'] = 'Время создания';
$_['ik_inv_prc'] = 'Время проведения';
$_['ik_inv_st'] = 'Состояние';
$_['ik_inv_st_success'] = 'Успешный';
$_['ik_inv_st_fail'] = 'Неуспешный';
$_['ik_inv_st_spending'] = 'В обработке';
$_['ik_ps_price'] = 'Сумма платежа в платежной системе';
$_['ik_co_rfn'] = 'Сумма зачисления';
