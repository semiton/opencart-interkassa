<?php

/**
 * Class ModelPaymentInterkassa
 *
 * @property DB db
 * @property Loader load
 * @property ModelSettingSetting model_setting_setting
 */
class ModelPaymentInterkassa extends Model
{
    const DEFAULT_CHARSET = 'utf-8';
    const DEFAULT_CALLBACK_METHOD = 'POST';
    const DEFAULT_ACTION = 'https://sci.interkassa.com/';

	public function install()
    {
		$this->db->query("
			CREATE TABLE `".DB_PREFIX."interkassa_payments` (
			    `order_id` int(11) NOT NULL,
				`ik_pm_no` bigint(19) unsigned NOT NULL AUTO_INCREMENT,
				`ik_co_id` varchar(25) NOT NULL,
				`ik_inv_id` varchar(100) DEFAULT NULL,
				`ik_trn_id` varchar(100) DEFAULT NULL,
				`ik_cur` varchar(3) DEFAULT NULL,
				`ik_am` decimal NOT NULL DEFAULT '0',
				`ik_ps_price` decimal NOT NULL DEFAULT '0',
				`ik_co_rfn` decimal NOT NULL DEFAULT '0',
				`ik_desc` text DEFAULT NULL,
				`ik_inv_crt` datetime DEFAULT NULL,
				`ik_inv_prc` datetime DEFAULT NULL,
				`ik_pw_via` varchar(62) DEFAULT NULL,
				`ik_inv_st` enum('success','fail','pending') NOT NULL DEFAULT 'pending',
				KEY `order_id` (`order_id`),
				PRIMARY KEY `ik_pm_no` (`ik_pm_no`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
		");

        $this->load->model('setting/setting');
        $config = $this->model_setting_setting->getSetting('interkassa');
        if (count($config) === 0) {
            $config = array(
                'interkassa_charset' => self::DEFAULT_CHARSET,
                'interkassa_callback_method' => self::DEFAULT_CALLBACK_METHOD,
                'interkassa_action' => self::DEFAULT_ACTION,
            );
            $this->model_setting_setting->editSetting('interkassa', $config);
        }
	}

	public function uninstall()
    {
		$this->db->query("DROP TABLE IF EXISTS `".DB_PREFIX."interkassa_payments`;");
	}

    /**
     * @param null|array $where
     * @return int
     */
    public function getPaymentsTotal($where = null)
    {
        $sql = "
            SELECT COUNT(`".DB_PREFIX."interkassa_payments`.`ik_pm_no`) AS `total` FROM `".DB_PREFIX."interkassa_payments`
            INNER JOIN `".DB_PREFIX."order` ON `".DB_PREFIX."interkassa_payments`.`order_id` = `".DB_PREFIX."order`.`order_id`";
        if ($where !== null) {
            $sql .= " WHERE ".$where;
        }

        $query = $this->db->query($sql);

        return (int) $query->row['total'];
    }

    /**
     * @param string|null $where
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return object
     */
    public function _getPayments($where = null, array $order = array('ik_pm_no' => 'DESC'), $limit = 20, $offset = 0)
    {
        $sql = "
            SELECT `".DB_PREFIX."interkassa_payments`.*, `".DB_PREFIX."order`.* FROM `".DB_PREFIX."interkassa_payments`
            INNER JOIN `".DB_PREFIX."order` ON `".DB_PREFIX."interkassa_payments`.`order_id` = `".DB_PREFIX."order`.`order_id`";
        if ($where !== null) {
            $sql .= " WHERE ".$where;
        }
        $order_count = count($order);
        if ($order_count > 0) {
            $sql .= " ORDER BY ";
            $i = 1;
            foreach ($order as $order_by => $sort) {
                $sql .= $order_by." ".$sort.($i < $order_count ? ", " : "");
            }
        }
        $sql .= " LIMIT ".(int) $limit." OFFSET ".(int) $offset;

        return $this->db->query($sql);
    }

    /**
     * @param string|null $where
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getPayments($where = null, array $order = array('ik_pm_no' => 'DESC'), $limit = 20, $offset = 0)
    {
        return $this->_getPayments($where, $order, $limit, $offset)->rows;
    }

    /**
     * @param int $order_id
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getPaymentsByOrderId($order_id, array $order = array('ik_pm_no' => 'DESC'), $limit = 20, $offset = 0)
    {
        return $this->getPayments(DB_PREFIX."interkassa_payments`.`order_id` = '".$this->db->escape($order_id)."'", $order, $limit, $offset);
    }
}
